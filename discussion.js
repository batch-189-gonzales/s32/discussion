let http = require('http');

let port = 4000;

let server = http.createServer(function(request, response) {
	if(request.url == '/items' && request.method == 'GET'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end('Data retrieved from the database.');
	}

	if(request.url == '/items' && request.method == 'POST'){
		response.writeHead(200, {'Content-type': 'text/plain'});
		response.end('Data sent to the database.');
	}
});

server.listen(port);

console.log(`Server is running at localhost:${port}`);